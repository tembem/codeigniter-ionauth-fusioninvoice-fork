<script type="text/javascript">
    $(function()
    {
        show_fields();
        
        $('#group_type').change(function()
        {
            show_fields();
        });

        function show_fields()
        {
            $('#administrator_fields').hide();
            $('#guest_fields').hide();

            group_type = $('#group_type').val();

            if (group_type == 1)
            {
                $('#administrator_fields').show();
            }
            else if (group_type == 2)
            {
                $('#guest_fields').show();
            }
        }
    });
</script>


<form method="post" class="form-horizontal">

    <div class="headerbar">
        <h1><?php echo lang('group_form'); ?></h1>
        <?php echo $this->layout->load_view('layout/header_buttons'); ?>
    </div>

    <div class="content">

        <?php echo $this->layout->load_view('layout/alerts'); ?>

        <div id="groupInfo">

            <fieldset>
                <legend>Group Information</legend>

                <div class="control-group">
                    <label class="control-label">Name: </label>
                    <div class="controls">
                        <input type="text" name="name" id="name" value="<?php echo $this->mdl_groups->form_value('name'); ?>">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Description: </label>
                    <div class="controls">
                        <input type="text" name="description" id="description" value="<?php echo $this->mdl_groups->form_value('description'); ?>">
                    </div>
                </div>

                

            </fieldset>

           

        </div>

    </div>

</form>
