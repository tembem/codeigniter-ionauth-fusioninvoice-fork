<div class="headerbar">
	<h1>Groups</h1>

	<div class="pull-right">
		<a class="btn btn-primary" href="<?php echo site_url('groups/form'); ?>"><i class="icon-plus icon-white"></i> <?php echo lang('new'); ?></a>
	</div>
	
	<div class="pull-right">
		<?php echo pager(site_url('groups/index'), 'mdl_groups'); ?>
	</div>

</div>

<?php echo $this->layout->load_view('layout/alerts'); ?>

<table class="table table-striped">

	<thead>
		<tr>
			<th>Name</th>
            <th>Description</th>
            <th><?php echo lang('options'); ?></th>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($groups as $group) { ?>
		<tr>
			<td><?php echo $group->name; ?></td>
			<td><?php echo $group->description; ?></td>
			<td>
				<div class="options btn-group">
					<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i> <?php echo lang('options'); ?></a>
					<ul class="dropdown-menu">
						<li>
							<a href="<?php echo site_url('groups/form/' . $group->id); ?>">
								<i class="icon-pencil"></i> <?php echo lang('edit'); ?>
							</a>
						</li>
                        <?php if ($group->id <> 1) { ?>
						<li>
							<a href="<?php echo site_url('groups/delete/' . $group->id); ?>" onclick="return confirm('<?php echo lang('delete_record_warning'); ?>');">
								<i class="icon-trash"></i> <?php echo lang('delete'); ?>
							</a>
						</li>
                        <?php } ?>
					</ul>
				</div>
			</td>
		</tr>
		<?php } ?>
	</tbody>

</table>
