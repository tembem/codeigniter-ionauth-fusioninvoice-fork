<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * FusionInvoice
 * 
 * A free and open source web based invoicing system
 *
 * @package		FusionInvoice
 * @author		Jesse Terry
 * @copyright	Copyright (c) 2012 - 2013, Jesse Terry
 * @license		http://www.fusioninvoice.com/support/page/license-agreement
 * @link		http://www.fusioninvoice.com
 * 
 */

class Mdl_Groups extends Response_Model {

    public $table               = 'groups';
    public $primary_key         = 'groups.id';
    public $date_created_field  = 'user_date_created';
    public $date_modified_field = 'user_date_modified';

    public function default_select()
    {
        $this->db->select('SQL_CALC_FOUND_ROWS groups.*', FALSE);
    }

    public function default_join()
    {
        //$this->db->join('fi_user_custom', 'fi_user_custom.user_id = fi_groups.user_id', 'left');
    }

    public function default_order_by()
    {
        $this->db->order_by('groups.name');
    }

    public function validation_rules()
    {
        return array(
            'name'      => array(
                'field' => 'name',
                'label' => 'Group Name',
                'rules' => 'required'
            ),
            
            'description' => array(
                'field' => 'description'
            )            
        );
    }

    public function validation_rules_existing()
    {
        return array(
            'name'      => array(
                'field' => 'name',
                'label' => 'Group Name',
                'rules' => 'required'
            ),
            
            'description' => array(
                'field' => 'description'
            )            
        );
    }

    public function db_array()
    {
        $db_array = parent::db_array();

        return $db_array;
    }

    public function save($id = NULL, $db_array = NULL)
    {
        $id = parent::save($id, $db_array);

        return $id;
    }

    public function delete($id)
    {
        $this->load->model('users/mdl_users_groups');
        $this->mdl_users_groups->delete_group($id);
        
        parent::delete($id);

        $this->load->helper('orphan');
        delete_orphans();
    }

}

?>
