<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * FusionInvoice
 * 
 * A free and open source web based invoicing system
 *
 * @package		FusionInvoice
 * @author		Jesse Terry
 * @copyright	Copyright (c) 2012 - 2013, Jesse Terry
 * @license		http://www.fusioninvoice.com/support/page/license-agreement
 * @link		http://www.fusioninvoice.com
 * 
 */

class Groups extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('mdl_groups');
    }

    public function index($page = 0)
    {
        $this->mdl_groups->paginate(site_url('groups/index'), $page);
        $groups = $this->mdl_groups->result();
        
        $this->layout->set('groups', $groups);
        //$this->layout->set('user_types', $this->mdl_groups->user_types());
        $this->layout->buffer('content', 'groups/index');
        $this->layout->render();
    }

    public function form($id = NULL)
    {
        if ($this->input->post('btn_cancel'))
        {
            redirect('groups');
        }

        if ($this->mdl_groups->run_validation(($id) ? 'validation_rules_existing' : 'validation_rules'))
        {
            $id = $this->mdl_groups->save($id);

            //$this->load->model('custom_fields/mdl_user_custom');

            //$this->mdl_user_custom->save_custom($id, $this->input->post('custom'));

            redirect('groups');
        }

        if ($id and !$this->input->post('btn_submit'))
        {
            $this->mdl_groups->prep_form($id);

            //$this->load->model('custom_fields/mdl_user_custom');

            //$user_custom = $this->mdl_user_custom->where('user_id', $id)->get();
			
			/*
            if ($user_custom->num_rows())
            {
                $user_custom = $user_custom->row();

                unset($user_custom->user_id, $user_custom->user_custom_id);

                foreach ($user_custom as $key => $val)
                {
                    $this->mdl_groups->set_form_value('custom[' . $key . ']', $val);
                }
            }
            */
        }
        elseif ($this->input->post('btn_submit'))
        {
            /*
            if ($this->input->post('custom'))
            {
                foreach ($this->input->post('custom') as $key => $val)
                {
                    $this->mdl_groups->set_form_value('custom[' . $key . ']', $val);
                }
            }
            */
        }
		
		/*
        $this->load->model('groups/mdl_user_clients');
        $this->load->model('clients/mdl_clients');
        $this->load->model('custom_fields/mdl_custom_fields');

        $this->layout->set(
            array(
                'id' => $id,
                'user_types' => $this->mdl_groups->user_types(),
                'user_clients' => $this->mdl_user_clients->where('fi_user_clients.user_id', $id)->get()->result(),
                'custom_fields' => $this->mdl_custom_fields->by_table('fi_user_custom')->get()->result()
            )
        );

        $this->layout->buffer('user_client_table', 'groups/partial_user_client_table');
        $this->layout->buffer('modal_user_client', 'groups/modal_user_client');
        */
        $this->layout->buffer('content', 'groups/form');
        $this->layout->render();
    }  

    public function delete($id)
    {
        if ($id <> 1)
        {
            $this->mdl_groups->delete($id);
        }
        redirect('groups');
    }

}

?>
