<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * FusionInvoice
 * 
 * A free and open source web based invoicing system
 *
 * @package		FusionInvoice
 * @author		Jesse Terry
 * @copyright	Copyright (c) 2012 - 2013, Jesse Terry
 * @license		http://www.fusioninvoice.com/support/page/license-agreement
 * @link		http://www.fusioninvoice.com
 * 
 */

class Mdl_Users_Groups extends MY_Model {
    
    public $table = 'users_groups';
    public $primary_key = 'users_groups.id';
    
    public function save_custom($user_id, $group_id)
    {    
        $this->db->select('id');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('users_groups');
        if ($query->num_rows() > 0)
	{
	    	$row = $query->row(); 
	
	    	$users_groups_id = $row->id;
        
        	parent::delete($users_groups_id);
	}
               
        
        if(!empty($group_id))
        {
        	$db_array['user_id'] = $user_id;
        	$db_array['group_id'] = $group_id;

       		parent::save(NULL, $db_array);
        }
    }
    
    public function delete_group($group_id)
    {
		$this->db->select('id');
        $this->db->where('group_id', $group_id);
        $query = $this->db->get('users_groups');
        if ($query->num_rows() > 0)
	{
	    	$row = $query->row();
        	$users_groups_id = $row->id;
        
        	parent::delete($users_groups_id);
        }
}
	
	public function delete_user($user_id)
    {
		$this->db->select('id');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('users_groups');
        if ($query->num_rows() > 0)
	{
	    	$row = $query->row();
        	$users_groups_id = $row->id;
        
        	parent::delete($users_groups_id);
        }
	}
    
}

?>