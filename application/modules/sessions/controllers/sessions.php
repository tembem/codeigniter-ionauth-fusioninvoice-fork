<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * FusionInvoice
 * 
 * A free and open source web based invoicing system
 *
 * @package		FusionInvoice
 * @author		Jesse Terry
 * @copyright	Copyright (c) 2012 - 2013, Jesse Terry
 * @license		http://www.fusioninvoice.com/support/page/license-agreement
 * @link		http://www.fusioninvoice.com
 * 
 */

class Sessions extends Base_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->lang->load('auth');
        $this->load->library('form_validation');
    }
    
    public function index()
    {
        if (!$this->ion_auth->logged_in())
        {
        	redirect('sessions/login');
        }
        else
        {
        	redirect('dashboard');
        }
    }

    public function login()
    {
        if ($this->input->post('btn_login'))
        {            
            //if ($this->authenticate($this->input->post('email'), $this->input->post('password')))
            if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), FALSE))
            {
				$this->session->set_flashdata('message', $this->ion_auth->messages());		
		
                //$this->session->set_userdata(array('user_type'=>1, 'user_id'=>1));                
                if ($this->session->userdata('user_type') == 1)
                {
                    redirect('dashboard');
                }
                elseif ($this->session->userdata('user_type') == 2)
                {
                    redirect('guest');
                }
            }
            else
            {
            	$this->session->set_flashdata('message', $this->ion_auth->errors());
            	redirect('sessions/login', 'refresh');
            }
        }
	$data2['message'] = $this->session->flashdata('message');
	
        $this->load->view('session_login', $data2);
    }

    public function logout()
    {
        //$this->session->sess_destroy();
        $this->ion_auth->logout();

        redirect('sessions/login');
    }

    public function authenticate($email_address, $password)
    {
        $this->load->model('mdl_sessions');

        if ($this->mdl_sessions->auth($email_address, $password))
        {
            return TRUE;
        }

        return FALSE;
    }
    
   	public function forgot_password()
	{
		$this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		if ($this->form_validation->run() == false)		{
			
			$data2['email'] = array('name' => 'email',
				'id' => 'email',
			);
	
			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$data2['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
			}
			else
			{
				$data2['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}	
			
			$data2['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('forgot_password', $data2);
		}
		else
		{
			// get identity for that email
			$config_tables = $this->config->item('tables', 'ion_auth');
			$config_columns = $this->config->item('columns', 'ion_auth');
			
			$identity = $this->db->where($config_columns['users.email'], $this->input->post('email'))->limit('1')->get($config_tables['users'])->row();
			
			if (empty($identity))
			{
				$this->session->set_flashdata('message', 'Email <strong>'.$this->input->post('email').'</strong> is not recognized');
				redirect("sessions/forgot_password", 'refresh');
			}
			
			//run the forgotten password method to email an activation code to the user		
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

	
			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("sessions/login", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("sessions/forgot_password", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$data2['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$data2['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$data2['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$data2['min_password_length'].'}.*$',
				);
				$data2['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$data2['min_password_length'].'}.*$',
				);
				$data2['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$data2['csrf'] = $this->_get_csrf_nonce();
				$data2['code'] = $code;

				//render
				$this->_render_page('reset_password', $data2);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						$this->logout();
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('sessions/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("sessions/forgot_password", 'refresh');
		}
	}

	function _render_page($view, $data=null, $render=false)
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}
	
	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

}

?>
