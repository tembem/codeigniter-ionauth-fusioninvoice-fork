<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>FusionInvoice</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<link href="<?php echo base_url(); ?>assets/default/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/default/css/style.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/default/js/libs/jquery-1.7.1.min.js"></script>
		<style>
			body {
				padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
			}
		</style>

		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        
        <script type="text/javascript">
            $(function() { $('#email').focus(); });
        </script>

	</head>

	<body>

	<div id="login">

		<h1>Reset Password</h1>
		
		<?php echo form_open('sessions/reset_password/' . $code, array('class'=>'form-horizontal'));?>

			<div class="control-group">
				<label class="control-label"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
				<div class="controls">
					<?php echo form_input($new_password);?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label"><?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?></label>
				<div class="controls">
					<?php echo form_input($new_password_confirm);?>
				</div>
			</div>
			<?php echo form_input($user_id);?>
			<?php echo form_hidden($csrf); ?>			
			<div class="control-group">
				<div class="controls">
					<div style="color:red"><?php echo $message;?></div>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<input type="submit" name="btn_reset" value="Change Now" class="btn btn-primary">
				</div>
			</div>

		</form>

	</div><!--end.container-->

	</body>
</html>