Hi,

You have requested to <?php echo strtolower(sprintf(lang('email_forgot_password_heading'), $identity));?>. <?php echo sprintf(lang('email_forgot_password_subheading'), anchor('sessions/reset_password/'. $user_forgotten_password_code, lang('email_forgot_password_link')));?>

Thank You,


Administrator
