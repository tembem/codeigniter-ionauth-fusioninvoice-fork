-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 16, 2013 at 12:28 PM
-- Server version: 5.5.23
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tobamart_fi2`
--

-- --------------------------------------------------------

--
-- Table structure for table `fi_clients`
--

CREATE TABLE IF NOT EXISTS `fi_clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_date_created` datetime NOT NULL,
  `client_date_modified` datetime NOT NULL,
  `client_name` varchar(100) NOT NULL,
  `client_address_1` varchar(100) DEFAULT '',
  `client_address_2` varchar(100) DEFAULT '',
  `client_city` varchar(45) DEFAULT '',
  `client_state` varchar(35) DEFAULT '',
  `client_zip` varchar(15) DEFAULT '',
  `client_country` varchar(35) DEFAULT '',
  `client_phone` varchar(20) DEFAULT '',
  `client_fax` varchar(20) DEFAULT '',
  `client_mobile` varchar(20) DEFAULT '',
  `client_email` varchar(100) DEFAULT '',
  `client_web` varchar(100) DEFAULT '',
  `client_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`client_id`),
  KEY `client_active` (`client_active`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `fi_clients`
--

INSERT INTO `fi_clients` (`client_id`, `client_date_created`, `client_date_modified`, `client_name`, `client_address_1`, `client_address_2`, `client_city`, `client_state`, `client_zip`, `client_country`, `client_phone`, `client_fax`, `client_mobile`, `client_email`, `client_web`, `client_active`) VALUES
(1, '2013-04-13 22:14:15', '2013-04-13 22:14:15', 'Bapak', '', '', '', '', '', '', '', '', '', '', '', 1),
(2, '2013-04-14 02:31:49', '2013-04-14 02:31:49', 'Robin', '', '', '', '', '', '', '', '', '', '', '', 1),
(3, '2013-04-14 02:35:56', '2013-04-14 02:35:56', 'Robin2', '', '', '', '', '', '', '', '', '', '', '', 1),
(4, '2013-04-14 02:40:42', '2013-04-14 02:40:42', 'BION', '', '', '', '', '', '', '', '', '', '', '', 1),
(5, '2013-04-14 02:41:13', '2013-04-14 02:41:13', 'HUJ', '', '', '', '', '', '', '', '', '', '', '', 1),
(6, '2013-04-14 02:42:31', '2013-04-14 02:42:31', 'BOMSS', '', '', '', '', '', '', '', '', '', '', '', 1),
(7, '2013-04-14 02:43:23', '2013-04-14 02:43:23', 'tre', '', '', '', '', '', '', '', '', '', '', '', 1),
(8, '2013-04-14 02:43:56', '2013-04-14 02:43:56', 'reew', '', '', '', '', '', '', '', '', '', '', '', 1),
(9, '2013-04-14 03:18:44', '2013-04-14 03:18:44', 'DONO', '', '', '', '', '', '', '', '', '', '', '', 1),
(10, '2013-04-14 03:40:28', '2013-04-14 03:40:28', 'ROBIN32', '', '', '', '', '', '', '', '', '', '', '', 1),
(11, '2013-04-14 23:59:43', '2013-04-14 23:59:43', 'ROGER2', '', '', '', '', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fi_client_custom`
--

CREATE TABLE IF NOT EXISTS `fi_client_custom` (
  `client_custom_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`client_custom_id`),
  KEY `client_id` (`client_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fi_client_custom`
--

INSERT INTO `fi_client_custom` (`client_custom_id`, `client_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fi_client_notes`
--

CREATE TABLE IF NOT EXISTS `fi_client_notes` (
  `client_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `client_note_date` date NOT NULL,
  `client_note` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`client_note_id`),
  KEY `client_id` (`client_id`,`client_note_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_custom_fields`
--

CREATE TABLE IF NOT EXISTS `fi_custom_fields` (
  `custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_table` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `custom_field_label` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `custom_field_column` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`custom_field_id`),
  KEY `custom_field_table` (`custom_field_table`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_email_templates`
--

CREATE TABLE IF NOT EXISTS `fi_email_templates` (
  `email_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_template_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_template_body` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`email_template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_imports`
--

CREATE TABLE IF NOT EXISTS `fi_imports` (
  `import_id` int(11) NOT NULL AUTO_INCREMENT,
  `import_date` datetime NOT NULL,
  PRIMARY KEY (`import_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_import_details`
--

CREATE TABLE IF NOT EXISTS `fi_import_details` (
  `import_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `import_id` int(11) NOT NULL,
  `import_lang_key` varchar(35) NOT NULL,
  `import_table_name` varchar(35) NOT NULL,
  `import_record_id` int(11) NOT NULL,
  PRIMARY KEY (`import_detail_id`),
  KEY `import_id` (`import_id`,`import_record_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_invoices`
--

CREATE TABLE IF NOT EXISTS `fi_invoices` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoice_group_id` int(11) NOT NULL,
  `invoice_date_created` date NOT NULL,
  `invoice_date_modified` datetime NOT NULL,
  `invoice_date_due` date NOT NULL,
  `invoice_number` varchar(20) NOT NULL,
  `invoice_terms` longtext NOT NULL,
  `invoice_url_key` char(32) NOT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `user_id` (`user_id`,`client_id`,`invoice_group_id`,`invoice_date_created`,`invoice_date_due`,`invoice_number`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fi_invoices`
--

INSERT INTO `fi_invoices` (`invoice_id`, `user_id`, `client_id`, `invoice_group_id`, `invoice_date_created`, `invoice_date_modified`, `invoice_date_due`, `invoice_number`, `invoice_terms`, `invoice_url_key`) VALUES
(1, 12, 11, 1, '2013-04-16', '2013-04-15 00:00:12', '2013-05-16', '1', '', '167bff712366a377d21bcf8720f2feb1');

-- --------------------------------------------------------

--
-- Table structure for table `fi_invoices_recurring`
--

CREATE TABLE IF NOT EXISTS `fi_invoices_recurring` (
  `invoice_recurring_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `recur_start_date` date NOT NULL,
  `recur_end_date` date NOT NULL,
  `recur_frequency` char(2) NOT NULL,
  `recur_next_date` date NOT NULL,
  PRIMARY KEY (`invoice_recurring_id`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_invoice_amounts`
--

CREATE TABLE IF NOT EXISTS `fi_invoice_amounts` (
  `invoice_amount_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `invoice_item_subtotal` decimal(10,2) DEFAULT '0.00',
  `invoice_item_tax_total` decimal(10,2) DEFAULT '0.00',
  `invoice_tax_total` decimal(10,2) DEFAULT '0.00',
  `invoice_total` decimal(10,2) DEFAULT '0.00',
  `invoice_paid` decimal(10,2) DEFAULT '0.00',
  `invoice_balance` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`invoice_amount_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `invoice_paid` (`invoice_paid`,`invoice_balance`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fi_invoice_amounts`
--

INSERT INTO `fi_invoice_amounts` (`invoice_amount_id`, `invoice_id`, `invoice_item_subtotal`, `invoice_item_tax_total`, `invoice_tax_total`, `invoice_total`, `invoice_paid`, `invoice_balance`) VALUES
(1, 1, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `fi_invoice_custom`
--

CREATE TABLE IF NOT EXISTS `fi_invoice_custom` (
  `invoice_custom_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  PRIMARY KEY (`invoice_custom_id`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_invoice_groups`
--

CREATE TABLE IF NOT EXISTS `fi_invoice_groups` (
  `invoice_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_group_name` varchar(50) NOT NULL DEFAULT '',
  `invoice_group_prefix` varchar(10) NOT NULL DEFAULT '',
  `invoice_group_next_id` int(11) NOT NULL,
  `invoice_group_left_pad` int(2) NOT NULL DEFAULT '0',
  `invoice_group_prefix_year` int(1) NOT NULL DEFAULT '0',
  `invoice_group_prefix_month` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invoice_group_id`),
  KEY `invoice_group_next_id` (`invoice_group_next_id`),
  KEY `invoice_group_left_pad` (`invoice_group_left_pad`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `fi_invoice_groups`
--

INSERT INTO `fi_invoice_groups` (`invoice_group_id`, `invoice_group_name`, `invoice_group_prefix`, `invoice_group_next_id`, `invoice_group_left_pad`, `invoice_group_prefix_year`, `invoice_group_prefix_month`) VALUES
(1, 'Invoice Default', '', 2, 0, 0, 0),
(2, 'Quote Default', 'QUO', 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fi_invoice_items`
--

CREATE TABLE IF NOT EXISTS `fi_invoice_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `item_tax_rate_id` int(11) NOT NULL DEFAULT '0',
  `item_date_added` date NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_description` longtext NOT NULL,
  `item_quantity` decimal(10,2) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_order` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `invoice_id` (`invoice_id`,`item_tax_rate_id`,`item_date_added`,`item_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_invoice_item_amounts`
--

CREATE TABLE IF NOT EXISTS `fi_invoice_item_amounts` (
  `item_amount_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_subtotal` decimal(10,2) NOT NULL,
  `item_tax_total` decimal(10,2) NOT NULL,
  `item_total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`item_amount_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_invoice_tax_rates`
--

CREATE TABLE IF NOT EXISTS `fi_invoice_tax_rates` (
  `invoice_tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `include_item_tax` int(1) NOT NULL DEFAULT '0',
  `invoice_tax_rate_amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`invoice_tax_rate_id`),
  KEY `invoice_id` (`invoice_id`,`tax_rate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_merchant_responses`
--

CREATE TABLE IF NOT EXISTS `fi_merchant_responses` (
  `merchant_response_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `merchant_response_date` date NOT NULL,
  `merchant_response_driver` varchar(35) NOT NULL,
  `merchant_response` varchar(255) NOT NULL,
  `merchant_response_reference` varchar(255) NOT NULL,
  PRIMARY KEY (`merchant_response_id`),
  KEY `merchant_response_date` (`merchant_response_date`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_payments`
--

CREATE TABLE IF NOT EXISTS `fi_payments` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL DEFAULT '0',
  `payment_date` date NOT NULL,
  `payment_amount` decimal(10,2) NOT NULL,
  `payment_note` longtext NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `payment_method_id` (`payment_method_id`),
  KEY `payment_amount` (`payment_amount`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_payment_custom`
--

CREATE TABLE IF NOT EXISTS `fi_payment_custom` (
  `payment_custom_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_custom_id`),
  KEY `payment_id` (`payment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_payment_methods`
--

CREATE TABLE IF NOT EXISTS `fi_payment_methods` (
  `payment_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_method_name` varchar(35) NOT NULL,
  PRIMARY KEY (`payment_method_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_quotes`
--

CREATE TABLE IF NOT EXISTS `fi_quotes` (
  `quote_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoice_group_id` int(11) NOT NULL,
  `quote_date_created` date NOT NULL,
  `quote_date_modified` datetime NOT NULL,
  `quote_date_expires` date NOT NULL,
  `quote_number` varchar(20) NOT NULL,
  `quote_url_key` char(32) NOT NULL,
  PRIMARY KEY (`quote_id`),
  KEY `user_id` (`user_id`,`client_id`,`invoice_group_id`,`quote_date_created`,`quote_date_expires`,`quote_number`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fi_quotes`
--

INSERT INTO `fi_quotes` (`quote_id`, `invoice_id`, `user_id`, `client_id`, `invoice_group_id`, `quote_date_created`, `quote_date_modified`, `quote_date_expires`, `quote_number`, `quote_url_key`) VALUES
(1, 1, 12, 11, 2, '2013-04-14', '2013-04-14 23:59:43', '2013-04-29', 'QUO1', '8f1eb5d3ff5004d80e8362b28b9d3cec');

-- --------------------------------------------------------

--
-- Table structure for table `fi_quote_amounts`
--

CREATE TABLE IF NOT EXISTS `fi_quote_amounts` (
  `quote_amount_id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) NOT NULL,
  `quote_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`quote_amount_id`),
  KEY `quote_id` (`quote_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fi_quote_amounts`
--

INSERT INTO `fi_quote_amounts` (`quote_amount_id`, `quote_id`, `quote_total`) VALUES
(1, 1, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `fi_quote_custom`
--

CREATE TABLE IF NOT EXISTS `fi_quote_custom` (
  `quote_custom_id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) NOT NULL,
  PRIMARY KEY (`quote_custom_id`),
  KEY `quote_id` (`quote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_quote_items`
--

CREATE TABLE IF NOT EXISTS `fi_quote_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) NOT NULL,
  `item_date_added` date NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_description` longtext NOT NULL,
  `item_quantity` decimal(10,2) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_order` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `quote_id` (`quote_id`,`item_date_added`,`item_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_quote_item_amounts`
--

CREATE TABLE IF NOT EXISTS `fi_quote_item_amounts` (
  `item_amount_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`item_amount_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_settings`
--

CREATE TABLE IF NOT EXISTS `fi_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(50) NOT NULL,
  `setting_value` longtext NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `setting_key` (`setting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `fi_settings`
--

INSERT INTO `fi_settings` (`setting_id`, `setting_key`, `setting_value`) VALUES
(1, 'default_language', 'english'),
(2, 'date_format', 'm/d/Y'),
(3, 'currency_symbol', '$'),
(4, 'currency_symbol_placement', 'before'),
(5, 'invoices_due_after', '30'),
(6, 'quotes_expire_after', '15'),
(7, 'default_invoice_group', '1'),
(8, 'default_quote_group', '2'),
(9, 'default_invoice_template', 'default_invoice'),
(10, 'default_quote_template', 'default_quote'),
(11, 'thousands_separator', ','),
(12, 'decimal_point', '.'),
(13, 'cron_key', 'MDr5tdiuWiVARu4A'),
(14, 'tax_rate_decimal_places', '2');

-- --------------------------------------------------------

--
-- Table structure for table `fi_tax_rates`
--

CREATE TABLE IF NOT EXISTS `fi_tax_rates` (
  `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_rate_name` varchar(25) NOT NULL,
  `tax_rate_percent` decimal(5,2) NOT NULL,
  PRIMARY KEY (`tax_rate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_users`
--

CREATE TABLE IF NOT EXISTS `fi_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(1) NOT NULL DEFAULT '0',
  `user_date_created` datetime NOT NULL,
  `user_date_modified` datetime NOT NULL,
  `user_name` varchar(100) DEFAULT '',
  `user_company` varchar(100) DEFAULT '',
  `user_address_1` varchar(100) DEFAULT '',
  `user_address_2` varchar(100) DEFAULT '',
  `user_city` varchar(45) DEFAULT '',
  `user_state` varchar(35) DEFAULT '',
  `user_zip` varchar(15) DEFAULT '',
  `user_country` varchar(35) DEFAULT '',
  `user_phone` varchar(20) DEFAULT '',
  `user_fax` varchar(20) DEFAULT '',
  `user_mobile` varchar(20) DEFAULT '',
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_web` varchar(100) DEFAULT '',
  `user_psalt` char(22) NOT NULL,
  `user_ip_address` varchar(100) DEFAULT '',
  `user_activation_code` varchar(40) DEFAULT '',
  `user_forgotten_password_code` varchar(40) DEFAULT '',
  `user_forgotten_password_time` int(11) unsigned DEFAULT '0',
  `user_remember_code` varchar(40) DEFAULT '',
  `user_last_login` int(11) unsigned DEFAULT '0',
  `user_active` tinyint(1) unsigned DEFAULT '1',
  `user_first_name` varchar(50) DEFAULT '',
  `user_last_name` varchar(50) DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `fi_users`
--

INSERT INTO `fi_users` (`user_id`, `user_type`, `user_date_created`, `user_date_modified`, `user_name`, `user_company`, `user_address_1`, `user_address_2`, `user_city`, `user_state`, `user_zip`, `user_country`, `user_phone`, `user_fax`, `user_mobile`, `user_email`, `user_password`, `user_web`, `user_psalt`, `user_ip_address`, `user_activation_code`, `user_forgotten_password_code`, `user_forgotten_password_time`, `user_remember_code`, `user_last_login`, `user_active`, `user_first_name`, `user_last_name`) VALUES
(1, 1, '0000-00-00 00:00:00', '2013-04-13 23:04:17', 'administrator', '', '', '', '', '', '', '', '', '', '', 'admin@admin.com', '$2a$10$1deedfdf9deedc333f624OjNfe38Itz7U69uPCZPHxF8NhthD0QGe', '', '1deedfdf9deedc333f624d', '', '', '', 0, '', 1365864090, 1, '', ''),
(2, 1, '2013-04-13 18:06:36', '2013-04-14 03:17:39', 'Jimmy Chandra', '', 'sa', 'sa2', 'ci', 'st', 'zip', 'coun', 'pon', 'fax', 'mob', 'jimmycdinata@gmail.com', '2ac8ADvdpdYUg', 'web', '01492ab8c8af3f0eda62c8', '', '', NULL, NULL, NULL, 1365877490, 1, '', ''),
(14, 1, '2013-04-15 07:39:20', '2013-04-15 07:39:20', 'New Just Testing', '', '', '', '', '', '', '', '', '', '', 'info@vgsglobal.com', '2a3jG2Iz75uPs', '', 'c7fed4b3419331334cdfdb', '', '', '', 0, '', 0, 1, '', ''),
(13, 1, '2013-04-13 13:44:27', '2013-04-13 13:44:27', 'Demo', '', '', '', '', '', '', '', '', '', '', 'demo@email.com', '2ac8ADvdpdYUg', '', '6bc1fe62a00f670d668daf', '', '', '', 0, '', 0, 1, '', ''),
(12, 1, '2013-04-13 13:25:21', '2013-04-14 03:17:15', 'Fusion Invoice', '', '', '', '', '', '', '', '', '', '', 'demo@fusioninvoice.com', '2ac8ADvdpdYUg', '', '51cc16493f42ce1c560ead', '', '', '', 0, '', 1366083012, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `fi_user_clients`
--

CREATE TABLE IF NOT EXISTS `fi_user_clients` (
  `user_client_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`user_client_id`),
  KEY `user_id` (`user_id`,`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fi_user_custom`
--

CREATE TABLE IF NOT EXISTS `fi_user_custom` (
  `user_custom_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_custom_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `fi_user_custom`
--

INSERT INTO `fi_user_custom` (`user_custom_id`, `user_id`) VALUES
(1, 2),
(3, 1),
(14, 14),
(13, 13),
(12, 12);

-- --------------------------------------------------------

--
-- Table structure for table `fi_versions`
--

CREATE TABLE IF NOT EXISTS `fi_versions` (
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_date_applied` varchar(14) NOT NULL,
  `version_file` varchar(45) NOT NULL,
  `version_sql_errors` int(2) NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `version_date_applied` (`version_date_applied`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `fi_versions`
--

INSERT INTO `fi_versions` (`version_id`, `version_date_applied`, `version_file`, `version_sql_errors`) VALUES
(1, '1365737388', '000_1.0.sql', 0),
(2, '1365737396', '001_1.0.1.sql', 0),
(3, '1365737396', '002_1.0.2.sql', 0),
(4, '1365737396', '003_1.0.3.sql', 0),
(5, '1365737396', '004_1.0.4.sql', 0),
(6, '1365737396', '005_1.0.5.sql', 0),
(7, '1365737396', '006_1.0.6.sql', 0),
(8, '1365737396', '007_1.0.7.sql', 0),
(9, '1365737396', '008_1.0.8.sql', 0),
(10, '1365737396', '009_1.0.9.sql', 0),
(11, '1365737396', '010_1.1.0.sql', 0),
(12, '1365737396', '011_1.1.1.sql', 0),
(13, '1365737396', '012_1.1.2.sql', 0),
(14, '1365737396', '013_1.1.3.sql', 0),
(15, '1365737396', '014_1.1.4.sql', 0),
(16, '1365737396', '015_1.1.5.sql', 0),
(17, '1365737396', '016_1.1.6.sql', 0),
(18, '1365737396', '017_1.1.7.sql', 0),
(19, '1365737396', '018_1.1.8.sql', 0),
(20, '1365737396', '019_1.1.9.sql', 0),
(21, '1365737396', '020_1.2.0.sql', 0),
(22, '1365737396', '021_1.2.1.sql', 0),
(23, '1365737396', '022_1.2.2.sql', 0),
(24, '1365737396', '023_1.2.3.sql', 0),
(25, '1365737396', '024_1.2.4.sql', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_date_created` datetime NOT NULL,
  `user_date_modified` datetime NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `user_date_created`, `user_date_modified`, `name`, `description`) VALUES
(1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'admin', 'Administrator'),
(12, '2013-04-15 07:29:52', '2013-04-15 07:29:52', 'New Group', 'This a testing group');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(32, 2, 1),
(33, 14, 12),
(29, 13, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
